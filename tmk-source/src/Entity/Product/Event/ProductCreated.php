<?php

namespace App\Entity\Product\Event;

use App\Entity\Category\Category;
use App\Entity\Product\Product;

class ProductCreated
{
    public int $id;
    public string $title;
    public array $categories;
    public float $price;
    public ?int $eId = null;

    public function __construct(Product $product)
    {
        $this->id = $product->getId()->getValue();
        $this->title = $product->getTitle();
        $this->categories = $product->getCategories();
        $this->price = $product->getPrice();
        $this->eId = $product->getEId();
        $this->categories = array_map(fn(Category $category): string => $category->getTitle(),
            $product->getCategories());
    }
}