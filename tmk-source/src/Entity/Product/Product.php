<?php

declare(strict_types=1);

namespace App\Entity\Product;

use App\Entity\Category\Category;
use App\Entity\Product\Event\ProductCreated;
use App\Entity\Product\Event\ProductEdited;
use App\EventAggregator\AggregateRoot;
use App\EventAggregator\EventsTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="products", indexes={
 *     @ORM\Index(columns={"e_id"})},
 *     uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"e_id"})
 * })
 */
class Product implements AggregateRoot
{
    use EventsTrait;

    /**
     * @ORM\Column(type="product_id")
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\SequenceGenerator(sequenceName="products_seq", initialValue=1)
     * @ORM\Id
     */
    private Id $id;

    /**
     * @var Category[]|Collection
     * @ORM\ManyToMany(targetEntity="App\Entity\Category\Category")
     * @ORM\JoinTable(name="products_categories",
     *      joinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id", onDelete="cascade")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="cascade")}
     * )
     * @ORM\OrderBy({"title" = "ASC"})
     */
    private Collection $categories;

    /**
     * @ORM\Column(type="string", length=12, nullable=false)
     */
    private string $title;

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    private float $price;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $eId = null;

    /**
     * @ORM\Version()
     * @ORM\Column(type="integer")
     */
    private $version;

    public function __construct(Id $id, array $categories, string $title, float $price)
    {
        $this->id = $id;
        $this->title = $title;
        $this->categories = new ArrayCollection($categories);
        $this->price = $price;
    }

    /**
     * @return int|null
     */
    public function getEId(): ?int
    {
        return $this->eId;
    }

    /**
     * @param int|null $eId
     */
    public function setEId(?int $eId): void
    {
        $this->eId = $eId;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function changeTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function changePrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return Category[]
     */
    public function getCategories(): array
    {
        return $this->categories->toArray();
    }

    public function getId(): Id
    {
        return $this->id;
    }

    public function changeCategories(array $categories): void
    {
        $this->guardCategories($categories);

        $current = $this->getCategories();
        $new = $categories;

        $compare = static function (Category $a, Category $b): int {
            return $a->getId()->getValue() <=> $b->getId()->getValue();
        };

        foreach (array_udiff($current, $new, $compare) as $item) {
            $this->categories->removeElement($item);
        }

        foreach (array_udiff($new, $current, $compare) as $item) {
            $this->categories->add($item);
        }
    }

    private function guardCategories(array $categories): void
    {
        if (\count($categories) === 0) {
            throw new \DomainException('Set at least one category.');
        }
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist(): void
    {
        $this->recordEvent(new ProductCreated($this));
    }

    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate(): void
    {
        $this->recordEvent(new ProductEdited($this));
    }
}