<?php

declare(strict_types=1);

namespace App\Entity\Category;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="categories", indexes={
 *     @ORM\Index(columns={"e_id"})},
 *     uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"e_id"})
 * })
 */
class Category
{

    /**
     * @ORM\Column(type="category_id")
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\SequenceGenerator(sequenceName="categories_seq", initialValue=1)
     * @ORM\Id
     */
    private Id $id;

    /**
     * @ORM\Column(type="string", length=12, nullable=false)
     */
    private string $title;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $eId = null;

    /**
     * @ORM\Version()
     * @ORM\Column(type="integer")
     */
    private $version;

    public function __construct(Id $id, string $title)
    {
        $this->id = $id;
        $this->title = $title;
    }

    public function getId(): Id
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getEId(): ?int
    {
        return $this->eId ?: null;
    }

    /**
     * @param int|null $eId
     */
    public function setEId(?int $eId): void
    {
        $this->eId = $eId;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    public function edit(string $title, ?int $eId)
    {
        $this->title = $title;
        $this->eId = $eId;
    }

    /**
     * @param string $title
     */
    public function changeTitle(string $title): void
    {
        $this->title = $title;
    }
}