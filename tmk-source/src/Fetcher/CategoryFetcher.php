<?php

declare(strict_types=1);

namespace App\Fetcher;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\FetchMode;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

class CategoryFetcher
{
    private Connection $connection;
    private PaginatorInterface $paginator;

    /**
     * CategoryFetcher constructor.
     *
     * @param \Doctrine\DBAL\Connection               $connection
     * @param \Knp\Component\Pager\PaginatorInterface $paginator
     */
    public function __construct(Connection $connection, PaginatorInterface $paginator)
    {
        $this->connection = $connection;
        $this->paginator = $paginator;
    }

    /**
     * @param int    $page
     * @param int    $size
     * @param string $sort
     * @param string $direction
     *
     * @return PaginationInterface
     */
    public function all(int $page, int $size, string $sort, string $direction): PaginationInterface
    {
        $qb = $this->connection->createQueryBuilder()
            ->select(
                'c.id',
                'c.title',
                'c.e_id'
            )
            ->from('categories', 'c');

        if (!\in_array($sort, ['sort', 'id', 'title', 'e_id'], true)) {
            throw new \UnexpectedValueException('Cannot sort by ' . $sort);
        }

        $qb->orderBy($sort, $direction === 'desc' ? 'desc' : 'asc');

        return $this->paginator->paginate($qb, $page, $size);
    }

    public function checkCategories(): bool
    {
        $categories = $this->connection->createQueryBuilder()
            ->select('COUNT(p.id)')
            ->from('categories', 'p')
            ->execute()
            ->fetch();
        return $categories['count'] > 0;
    }

    public function listOfCategories(): array
    {
        $stmt = $this->connection->createQueryBuilder()
            ->select(
                'id',
                'title'
            )
            ->from('categories')
            ->orderBy('title')
            ->execute();

        return $stmt->fetchAll(\PDO::FETCH_KEY_PAIR);
    }

    public function findByEId(int $eId): ?int
    {
        $stmt = $this->connection->createQueryBuilder()
            ->select('c.id')
            ->from('categories', 'c')
            ->andWhere('c.e_id = :eId')
            ->setParameter(':eId', $eId)
            ->execute();

        $result = $stmt->fetch();

        return $result ? $result['id'] : null;
    }
}