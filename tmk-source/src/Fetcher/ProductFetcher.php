<?php

declare(strict_types=1);

namespace App\Fetcher;

use App\Entity\Product\Product;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\FetchMode;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

class ProductFetcher
{
    private Connection $connection;
    private PaginatorInterface $paginator;
    private ObjectRepository $repository;

    /**
     * ProductFetcher constructor.
     *
     * @param \Doctrine\DBAL\Connection               $connection
     * @param \Doctrine\ORM\EntityManagerInterface    $em
     * @param \Knp\Component\Pager\PaginatorInterface $paginator
     */
    public function __construct(Connection $connection, EntityManagerInterface $em, PaginatorInterface $paginator)
    {
        $this->connection = $connection;
        $this->paginator = $paginator;
        $this->repository = $em->getRepository(Product::class);
    }

    /**
     * @param int    $page
     * @param int    $size
     * @param string $sort
     * @param string $direction
     *
     * @return PaginationInterface
     */
    public function all(int $page, int $size, string $sort, string $direction): PaginationInterface
    {
        $qb = $this->connection->createQueryBuilder()
            ->select(
                'p.id',
                'p.title',
                'p.price',
                'p.e_id'
            )
            ->from('products', 'p');

        if (!\in_array($sort, ['sort', 'id', 'title', 'e_id'], true)) {
            throw new \UnexpectedValueException('Cannot sort by ' . $sort);
        }

        $qb->orderBy($sort, $direction === 'desc' ? 'desc' : 'asc');

        $pagination = $this->paginator->paginate($qb, $page, $size);

        $products = (array) $pagination->getItems();
        $categories = $this->batchLoadCategories(array_column($products, 'id'));

        $pagination->setItems(array_map(static function (array $product) use ($categories) {
            return array_merge($product, [
                'categories' => array_filter($categories, static function (array $category) use ($product) {
                    return $category['product_id'] === $product['id'];
                }),
            ]);
        }, $products));

        return $pagination;
    }

    private function batchLoadCategories(array $ids): array
    {
        $stmt = $this->connection->createQueryBuilder()
            ->select(
                'e.product_id',
                'c.title'
            )
            ->from('products_categories', 'e')
            ->innerJoin('e', 'categories', 'c', 'c.id = e.category_id')
            ->andWhere('e.product_id IN (:product)')
            ->setParameter(':product', $ids, Connection::PARAM_INT_ARRAY)
            ->orderBy('title')
            ->execute();

        return $stmt->fetchAll(FetchMode::ASSOCIATIVE);
    }
}