<?php

declare(strict_types=1);

namespace App\Command\Import;

use App\Repository\CategoryRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Service\Category\UseCase\Create;
use App\Service\Category\UseCase\Edit;

class ImportCategoriesCommand extends Command
{
    private ValidatorInterface $validator;
    private string $file;
    private CategoryRepository $categories;
    private Edit\Handler $editHandler;
    private Create\Handler $createHandler;

    public function __construct(
        string $file,
        ValidatorInterface $validator,
        CategoryRepository $categories,
        Edit\Handler $editHandler,
        Create\Handler $createHandler)
    {
        $this->categories = $categories;
        $this->validator = $validator;
        $this->file = $file;
        $this->editHandler = $editHandler;
        $this->createHandler = $createHandler;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setName('import:categories')
            ->setDescription('Import Categories');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!file_exists($this->file)) {
            throw new FileNotFoundException("File $this->file is not found.");
        }

        $categories = json_decode(file_get_contents($this->file), true);

        foreach ($categories as $category) {
            if ($existingCategory = $this->categories->findByEId($category['eId'])) {
                /** @var \App\Entity\Category\Category $existingCategory */
                $command = Edit\Command::fromImport($existingCategory, $category['title']);

                $handler = $this->editHandler;
            }
            else {
                $command = Create\Command::fromImport($category['title'], $category['eId']);

                $handler = $this->createHandler;
            }

            $violations = $this->validator->validate($command);

            if ($violations->count()) {
                $output->writeln('<error>' . "Unable import category $command->title. Errors:" . '</error>');
                foreach ($violations as $violation) {
                    $output->writeln('<error>' . $violation->getPropertyPath() . ': ' . $violation->getMessage() . '</error>');
                }
                continue;
            }
            else {
                $output->writeln('<comment>' . "Category $command->title imported successfully" . '</comment>');
            }

            $handler->handle($command);
        }

        $output->writeln('<info>Done!</info>');
        return 0;
    }
}