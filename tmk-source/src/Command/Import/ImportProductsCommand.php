<?php

declare(strict_types=1);

namespace App\Command\Import;

use App\Fetcher\CategoryFetcher;
use App\Repository\ProductRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Service\Product\UseCase\Create;
use App\Service\Product\UseCase\Edit;

class ImportProductsCommand extends Command
{
    private ValidatorInterface $validator;
    private string $file;
    private ProductRepository $products;
    private CategoryFetcher $categories;
    private Edit\Handler $editHandler;
    private Create\Handler $createHandler;

    public function __construct(
        string $file,
        ValidatorInterface $validator,
        ProductRepository $products,
        Edit\Handler $editHandler,
        Create\Handler $createHandler,
        CategoryFetcher $categories)
    {
        $this->products = $products;
        $this->validator = $validator;
        $this->file = $file;
        $this->editHandler = $editHandler;
        $this->createHandler = $createHandler;

        parent::__construct();
        $this->categories = $categories;
    }

    protected function configure(): void
    {
        $this
            ->setName('import:products')
            ->setDescription('Import Products');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!file_exists($this->file)) {
            throw new FileNotFoundException("File $this->file is not found.");
        }

        $products = json_decode(file_get_contents($this->file), true);

        foreach ($products as $product) {
            $categoriesEIds = $product['categoriesEId'] ?? $product['categoryEId']; // I don't know which option is correct. There are both in file.
            $categoriesIds = $this->findCategoriesIds($categoriesEIds, $output);

            if (!\count($categoriesIds)) {
                $output->writeln('<error>' . "Unable import product " . $product['title'] . ". All categories don't exsist." . '</error>');
                continue;
            }

            if ($existingProduct = $this->products->findByEId($product['eId'])) {
                /** @var \App\Entity\Product\Product $existingProduct */
                $command = Edit\Command::fromImport($existingProduct, $product['title'], $product['price'], $categoriesIds);

                $handler = $this->editHandler;
            }
            else {

                $command = Create\Command::fromImport($product['eId'], $product['title'], $product['price'], $categoriesIds);

                $handler = $this->createHandler;
            }

            $violations = $this->validator->validate($command);

            if ($violations->count()) {
                $output->writeln('<error>' . "Unable import product $command->title. Errors:" . '</error>');
                foreach ($violations as $violation) {
                    $output->writeln('<error>' . $violation->getPropertyPath() . ': ' . $violation->getMessage() . '</error>');
                }
                continue;
            }
            else {
                $output->writeln('<comment>' . "Product $command->title imported successfully" . '</comment>');
            }

            $handler->handle($command);

        }

        $output->writeln('<info>Done!</info>');
        return 0;
    }

    private function findCategoriesIds(array $categoriesEId, $output): array
    {
        $result = [];
        foreach ($categoriesEId as $eId) {
            if ($id = $this->categories->findByEId($eId)) {
                $result[] = $id;
            }
            else {
                $output->writeln('<comment>' . "Category with eId=$eId dosen't exsist" . '</comment>');
            }
        }
        return $result;
    }

}