<?php

declare(strict_types=1);

namespace App\Event\Listener\Product;

use App\Entity\Product\Event\ProductCreated;
use App\Entity\Product\Event\ProductEdited;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Twig\Environment;

class EmailNotificationSubscriber implements EventSubscriberInterface
{
    private MailerInterface  $mailer;
    private Environment $twig;
    private string $address;

    public function __construct(string $address, MailerInterface $mailer, Environment $twig)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->address = $address;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            ProductEdited::class  => 'onProductEdited',
            ProductCreated::class => 'onProductCreated'
        ];
    }

    public function onProductCreated(ProductCreated $event): void
    {
        $message = (new Email())
            ->subject("New Product $event->title was created!")
            ->to($this->address)
            ->html($this->twig->render('mail/product/product-created.html.twig', [
                'product' => $event,
            ]), 'text/html');

        $this->mailer->send($message);

    }

    public function onProductEdited(ProductEdited $event): void
    {
        $message = (new Email())
            ->subject("Product with id=$event->id was edited!")
            ->to($this->address)
            ->html($this->twig->render('mail/product/product-edited.html.twig', [
                'product' => $event,
            ]), 'text/html');

        $this->mailer->send($message);
    }

}