<?php

declare(strict_types=1);

namespace App\Service\Category\UseCase\Delete;

use App\Entity\Category\Id;
use App\Repository\CategoryRepository;
use App\Service\Flusher;

class Handler
{
    private CategoryRepository $categories;
    private Flusher $flusher;

    public function __construct(CategoryRepository $categories, Flusher $flusher)
    {
        $this->categories = $categories;
        $this->flusher = $flusher;
    }

    public function handle(Command $command): void
    {
        $category = $this->categories->get(new Id($command->id));

        if ($this->categories->hasProducts($command->id)) {
            throw new \DomainException('Unable to remove category with products.');
        }

        $this->categories->remove($category);

        $this->flusher->flush();
    }
}
