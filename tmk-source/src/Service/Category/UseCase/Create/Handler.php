<?php

declare(strict_types=1);

namespace App\Service\Category\UseCase\Create;

use App\Entity\Category\Category;
use App\Entity\Category\Id;
use App\Repository\CategoryRepository;
use App\Service\Flusher;

class Handler
{
    private CategoryRepository $categories;
    private Flusher $flusher;

    public function __construct(CategoryRepository $categories, Flusher $flusher)
    {
        $this->categories = $categories;
        $this->flusher = $flusher;
    }

    public function handle(Command $command): void
    {

        $title = $command->title;
        $eId = $command->eId ?? null;

        if ($this->categories->hasByTitle($title)) {
            throw new \DomainException('Category already exists.');
        }

        if ($eId && $this->categories->hasByEId($eId)) {
            throw new \DomainException("Category with eId=$eId already exists.");
        }

        $category = new Category(
            $this->categories->nextId(),
            $title
        );

        if ($eId) {
            $category->setEId($eId);
        }

        $this->categories->add($category);

        $this->flusher->flush();
    }
}
