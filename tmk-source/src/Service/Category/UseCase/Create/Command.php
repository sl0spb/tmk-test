<?php

declare(strict_types=1);

namespace App\Service\Category\UseCase\Create;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min="3", max="12")
     */
    public string $title = '';

    public ?int $eId = null;

    public static function fromImport(string $title ,int $eId): self
    {
        $command = new self();
        $command->title = $title;
        $command->eId = $eId;
        return $command;
    }
}
