<?php

declare(strict_types=1);

namespace App\Service\Category\UseCase\Edit;

use App\Entity\Category\Category;
use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @Assert\NotBlank()
     */
    public int $id;
    /**
     * @Assert\NotBlank()
     * @Assert\Length(min="3", max="12")
     */
    public string $title;

    public ?int $eId;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public static function fromCategory(Category $category): self
    {
        $command = new self($category->getId()->getValue());
        $command->title = $category->getTitle();
        $command->eId = $category->getEId();
        return $command;
    }

    public static function fromImport(Category $category, string $title): self
    {
        $command = new self($category->getId()->getValue());
        $command->title = $title;
        $command->eId = $category->getEId();
        return $command;
    }
}