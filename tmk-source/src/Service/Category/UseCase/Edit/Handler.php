<?php

declare(strict_types=1);

namespace App\Service\Category\UseCase\Edit;

use App\Entity\Category\Id;
use App\Repository\CategoryRepository;
use App\Service\Flusher;

class Handler
{
    private CategoryRepository $categories;
    private Flusher $flusher;

    public function __construct(CategoryRepository $projects, Flusher $flusher)
    {
        $this->categories = $projects;
        $this->flusher = $flusher;
    }

    public function handle(Command $command): void
    {
        $category = $this->categories->get(new Id($command->id));
        $title = $command->title;

        $eId = $command->eId ?? null;
        if ($this->categories->hasByTitle($title) && $category->getTitle() !== $title) {
            throw new \DomainException("Category with title $title already exists.");
        }

        if ($eId && $this->categories->hasByEId($eId) && $category->getEId() !== $eId) {
            throw new \DomainException("Category with eId=$eId already exists.");
        }

        $category->edit(
            $command->title,
            $command->eId
        );

        $this->flusher->flush();
    }
}