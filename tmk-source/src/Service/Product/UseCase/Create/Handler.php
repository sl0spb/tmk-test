<?php

declare(strict_types=1);

namespace App\Service\Product\UseCase\Create;

use App\Entity\Category\Category;
use App\Entity\Category\Id as CategoryId;
use App\Entity\Product\Product;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use App\Service\Flusher;

class Handler
{
    private ProductRepository $products;
    private CategoryRepository $categories;
    private Flusher $flusher;

    public function __construct(ProductRepository $products, Flusher $flusher, CategoryRepository $categories)
    {
        $this->products = $products;
        $this->flusher = $flusher;
        $this->categories = $categories;
    }

    public function handle(Command $command): void
    {
        $eId = $command->eId ?? null;

        if ($eId && $this->products->hasByEId($eId)) {
            throw new \DomainException("Product with eId=$eId already exists.");
        }

        $categories = array_map(fn(int $id): Category => $this->categories->get(new CategoryId($id)),
            $command->categories);

        $product = new Product(
            $this->products->nextId(),
            $categories,
            $command->title,
            $command->price,
        );

        if ($eId) {
            $product->setEId($eId);
        }

        $this->products->add($product);

        $this->flusher->flush($product);
    }
}
