<?php

declare(strict_types=1);

namespace App\Service\Product\UseCase\Create;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min="3", max="12")
     */
    public string $title = '';

    /**
     * @Assert\NotBlank()
     */
    public array $categories = [];

    /**
     * @Assert\NotBlank()
     * @Assert\Range(min="0", max="200")
     */
    public float $price = 0;

    public ?int $eId = null;

    public static function fromImport(int $eId, string $title, float $price, array $categoriesIds): self
    {
        $command = new self();
        $command->eId = $eId;
        $command->title = $title;
        $command->price = $price;
        $command->categories = $categoriesIds;

        return $command;
    }

}
