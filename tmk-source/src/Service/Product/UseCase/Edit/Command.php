<?php

declare(strict_types=1);

namespace App\Service\Product\UseCase\Edit;

use App\Entity\Category\Category;
use App\Entity\Product\Product;
use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @Assert\NotBlank()
     */
    public int $id;
    /**
     * @Assert\NotBlank()
     * @Assert\Length(min="3", max="12")
     */
    public string $title;

    /**
     * @Assert\NotBlank()
     */
    public array $categories;

    /**
     * @Assert\NotBlank()
     * @Assert\Range(min="0", max="200")
     */
    public float $price;

    public ?int $eId;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public static function fromProduct(Product $product): self
    {
        $command = new self($product->getId()->getValue());

        $command->title = $product->getTitle();
        $command->price = $product->getPrice();
        $command->eId = $product->getEId();

        $command->categories = array_map(fn(Category $category): int => $category->getId()->getValue(),
            $product->getCategories());

        return $command;
    }

    public static function fromImport(Product $existingProduct, string $title, float $price, array $categoriesIds): self
    {
        $command = new self($existingProduct->getId()->getValue());

        $command->title = $title;
        $command->price = $price;
        $command->eId = $existingProduct->getEId();
        $command->categories = $categoriesIds;

        return $command;
    }

}