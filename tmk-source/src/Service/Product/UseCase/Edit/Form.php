<?php

declare(strict_types=1);

namespace App\Service\Product\UseCase\Edit;

use App\Fetcher\CategoryFetcher;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Form extends AbstractType
{
    private CategoryFetcher $categories;

    public function __construct(CategoryFetcher $categories)
    {
        $this->categories = $categories;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', Type\TextType::class)
            ->add('categories', Type\ChoiceType::class, [
                'choices'  => array_flip($this->categories->listOfCategories()),
                'expanded' => true,
                'multiple' => true,
            ])
            ->add('price', Type\NumberType::class)
            ->add('eId', Type\IntegerType::class, ['required' => false]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Command::class,
        ]);
    }
}