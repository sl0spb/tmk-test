<?php

declare(strict_types=1);

namespace App\Service\Product\UseCase\Delete;

use App\Entity\Product\Product;
use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @Assert\NotBlank()
     */
    public int $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }
}
