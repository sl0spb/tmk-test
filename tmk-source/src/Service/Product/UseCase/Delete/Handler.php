<?php

declare(strict_types=1);

namespace App\Service\Product\UseCase\Delete;


use App\Entity\Product\Id;
use App\Entity\Product\Product;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use App\Service\Flusher;

class Handler
{
    private ProductRepository $products;
    private Flusher $flusher;

    public function __construct(ProductRepository $products, Flusher $flusher)
    {
        $this->products = $products;
        $this->flusher = $flusher;
    }

    public function handle(Command $command): void
    {
        $product = $this->products->get(new Id($command->id));

        $this->products->remove($product);

        $this->flusher->flush();
    }
}
