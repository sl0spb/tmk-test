<?php

namespace App\Controller;

use App\Entity\Category\Category;
use App\Fetcher\CategoryFetcher;
use App\Service\Category\UseCase\Create;
use App\Service\Category\UseCase\Edit;
use App\Service\Category\UseCase\Delete;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/category", name="category")
 */
class CategoryController extends AbstractController
{
    private const PER_PAGE = 25;

    private ErrorHandler $errors;
    private CategoryFetcher $categories;

    public function __construct(ErrorHandler $errors, CategoryFetcher $categories)
    {
        $this->errors = $errors;
        $this->categories = $categories;
    }

    /**
     * @Route("", name="")
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request): Response
    {
        $pagination = $this->categories->all(
            $request->query->getInt('page', 1),
            self::PER_PAGE,
            $request->query->get('sort', 'title'),
            $request->query->get('direction', 'asc')
        );

        return $this->render('app/category/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/create", name=".create")
     * @param Request        $request
     * @param Create\Handler $handler
     *
     * @return Response
     */
    public function create(Request $request, Create\Handler $handler): Response
    {
        $command = new Create\Command();

        $form = $this->createForm(Create\Form::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $handler->handle($command);
                return $this->redirectToRoute('category');
            } catch (\DomainException $e) {
                $this->errors->handle($e);
                $this->addFlash('error', $e->getMessage());
            }
        }

        return $this->render('app/category/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name=".edit")
     * @param Category     $category
     * @param Request      $request
     * @param Edit\Handler $handler
     *
     * @return Response
     */
    public function edit(Category $category, Request $request, Edit\Handler $handler): Response
    {
        $command = Edit\Command::fromCategory($category);

        $form = $this->createForm(Edit\Form::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $handler->handle($command);
                return $this->redirectToRoute('category');
            } catch (\DomainException $e) {
                $this->errors->handle($e);
                $this->addFlash('error', $e->getMessage());
            }
        }

        return $this->render('app/category/edit.html.twig', [
            'form'     => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/delete", name=".delete", methods={"POST"})
     * @param Category     $category
     * @param Request      $request
     * @param Delete\Handler $handler
     *
     * @return Response
     */
    public function delete(Category $category, Request $request, Delete\Handler $handler): Response
    {
        if (!$this->isCsrfTokenValid('delete', $request->request->get('token'))) {
            return $this->redirectToRoute('category');
        }

        $command = new Delete\Command($category->getId()->getValue());

        try {
            $handler->handle($command);
        } catch (\DomainException $e) {
            $this->errors->handle($e);
            $this->addFlash('error', $e->getMessage());
        }

        return $this->redirectToRoute('category');
    }
}