<?php

namespace App\Controller;

use App\Entity\Product\Product;
use App\Fetcher\CategoryFetcher;
use App\Fetcher\ProductFetcher;
use App\Service\Product\UseCase\Create;
use App\Service\Product\UseCase\Edit;
use App\Service\Product\UseCase\Delete;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/product", name="product")
 */
class ProductController extends AbstractController
{
    private const PER_PAGE = 25;

    private ErrorHandler $errors;
    private ProductFetcher $products;

    public function __construct(ErrorHandler $errors, ProductFetcher $products)
    {
        $this->errors = $errors;
        $this->products = $products;
    }

    /**
     * @Route("", name="")
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request): Response
    {
        $pagination = $this->products->all(
            $request->query->getInt('page', 1),
            self::PER_PAGE,
            $request->query->get('sort', 'title'),
            $request->query->get('direction', 'asc')
        );
        return $this->render('app/product/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/create", name=".create")
     * @param \App\Fetcher\CategoryFetcher $categories
     * @param Request                      $request
     * @param Create\Handler               $handler
     *
     * @return Response
     */
    public function create(CategoryFetcher $categories, Request $request, Create\Handler $handler): Response
    {

        if (!$categories->checkCategories()) {
            $this->addFlash('error', 'Add at least one category before adding products.');
            return $this->redirectToRoute('category.create');
        }

        $command = new Create\Command();

        $form = $this->createForm(Create\Form::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $handler->handle($command);
                return $this->redirectToRoute('product');
            } catch (\DomainException $e) {
                $this->errors->handle($e);
                $this->addFlash('error', $e->getMessage());
            }
        }

        return $this->render('app/product/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name=".edit")
     * @param Product      $product
     * @param Request      $request
     * @param Edit\Handler $handler
     *
     * @return Response
     */
    public function edit(Product $product, Request $request, Edit\Handler $handler): Response
    {
        $command = Edit\Command::fromProduct($product);

        $form = $this->createForm(Edit\Form::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $handler->handle($command);
                return $this->redirectToRoute('product');
            } catch (\DomainException $e) {
                $this->errors->handle($e);
                $this->addFlash('error', $e->getMessage());
            }
        }

        return $this->render('app/product/edit.html.twig', [
            'form'    => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/delete", name=".delete", methods={"POST"})
     * @param Product        $product
     * @param Request        $request
     * @param Delete\Handler $handler
     *
     * @return Response
     */
    public function delete(Product $product, Request $request, Delete\Handler $handler): Response
    {
        if (!$this->isCsrfTokenValid('delete', $request->request->get('token'))) {
            return $this->redirectToRoute('product');
        }

        $command = new Delete\Command($product->getId()->getValue());

        try {
            $handler->handle($command);
        } catch (\DomainException $e) {
            $this->errors->handle($e);
            $this->addFlash('error', $e->getMessage());
        }

        return $this->redirectToRoute('product');
    }
}