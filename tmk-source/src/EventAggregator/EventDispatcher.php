<?php

declare(strict_types=1);

namespace App\EventAggregator;

interface EventDispatcher
{
    public function dispatch(array $events): void;
}