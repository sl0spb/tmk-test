<?php

declare(strict_types=1);

namespace App\EventAggregator;

interface AggregateRoot
{
    public function releaseEvents(): array;
}