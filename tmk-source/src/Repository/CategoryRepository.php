<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Category\Category;
use App\Entity\Category\Id;
use App\Service\EntityNotFoundException;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;

class CategoryRepository
{
    private ObjectRepository $repo;
    private Connection $connection;
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em, Connection $connection)
    {
        $this->repo = $em->getRepository(Category::class);
        $this->em = $em;
        $this->connection = $connection;
    }

    public function get(Id $id): Category
    {
        /** @var Category $category */
        if (!$category = $this->repo->find($id->getValue())) {
            throw new EntityNotFoundException('Category is not found.');
        }
        return $category;
    }

    public function nextId(): Id
    {
        return new Id((int) $this->connection->query('SELECT nextval(\'categories_seq\')')->fetchColumn());
    }

    public function hasByTitle(string $title): bool
    {
        return $this->repo->createQueryBuilder('t')
                ->select('COUNT(t.id)')
                ->andWhere('t.title = :title')
                ->setParameter(':title', $title)
                ->getQuery()->getSingleScalarResult() > 0;
    }

    public function hasByEId(int $eId): bool
    {
        return $this->repo->createQueryBuilder('t')
                ->select('COUNT(t.id)')
                ->andWhere('t.eId = :eId')
                ->setParameter(':eId', $eId)
                ->getQuery()->getSingleScalarResult() > 0;
    }

    public function hasProducts(int $id): bool
    {
        $products = $this->connection->createQueryBuilder()
            ->select(
                'COUNT(p.id)'
            )
            ->from('products', 'p')
            ->innerJoin('p', 'products_categories', 'c', 'p.id = c.product_id')
            ->andWhere('c.category_id = :id')
            ->setParameter(':id', $id)
            ->execute()
            ->fetch();

        return $products['count'] > 0;

    }

    public function findByEId(int $eId)
    {
        $result = $this->repo->findOneBy(['eId' => $eId]);

        return $result ?: null;
    }

    public function add(Category $category): void
    {
        $this->em->persist($category);
    }

    public function remove(Category $category): void
    {
        $this->em->remove($category);
    }
}