<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Product\Id;
use App\Entity\Product\Product;
use App\Service\EntityNotFoundException;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;

class ProductRepository
{
    private ObjectRepository $repo;
    private Connection $connection;
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em, Connection $connection)
    {
        $this->repo = $em->getRepository(Product::class);
        $this->em = $em;
        $this->connection = $connection;
    }

    public function get(Id $id): Product
    {
        /** @var Product $product */
        if (!$product = $this->repo->find($id->getValue())) {
            throw new EntityNotFoundException('Category is not found.');
        }
        return $product;
    }

    public function nextId(): Id
    {
        return new Id((int) $this->connection->query('SELECT nextval(\'products_seq\')')->fetchColumn());
    }

    public function hasByEId(int $eId): bool
    {
        return $this->repo->createQueryBuilder('t')
                ->select('COUNT(t.id)')
                ->andWhere('t.eId = :eId')
                ->setParameter(':eId', $eId)
                ->getQuery()->getSingleScalarResult() > 0;
    }

    public function add(Product $product): void
    {
        $this->em->persist($product);
    }

    public function remove(Product $product): void
    {
        $this->em->remove($product);
    }

    public function findByEId(int $eId)
    {
        $result = $this->repo->findOneBy(['eId' => $eId]);

        return $result ?: null;
    }
}