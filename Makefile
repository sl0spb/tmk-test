up: docker-up
down: docker-down
restart: docker-down docker-up
init: docker-down-clear tmk-clear docker-pull docker-build docker-up tmk-init
require: tmk-composer-require
cc: cache-clear

docker-up:
	docker-compose up -d

docker-down:
	docker-compose down --remove-orphans

docker-down-clear:
	docker-compose down --remove-orphans

docker-pull:
	docker-compose pull

docker-build:
	docker-compose build

tmk-clear:
	docker run --rm -v ${PWD}/tmk-source:/app --workdir=/app alpine rm -f .ready

tmk-init: tmk-composer-install tmk-wait-db tmk-migrations tmk-ready

tmk-wait-db:
	until docker-compose exec -T tmk-postgres pg_isready --timeout=0 --dbname=tmk ; do sleep 1 ; done

tmk-migrations:
	docker-compose run --rm php-cli php bin/console doctrine:migrations:migrate --no-interaction

tmk-ready:
	docker run --rm -v ${PWD}/tmk-source:/app --workdir=/app alpine touch .ready

tmk-composer-install:
	docker-compose run --rm php-cli composer install

tmk-composer-require:
	docker-compose run --rm php-cli composer require $(app)

cache-clear:
	docker-compose run --rm php-cli php bin/console cache:clear